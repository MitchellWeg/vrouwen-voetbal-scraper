import os
import csv
import polars as pd

CLEANED_DIR="cleaned"
DATA_DIR="data"

def main():
    # create_teams_csv()
    create_matchups_csv()

def create_matchups_csv():
    teams = pd.read_csv(f"{CLEANED_DIR}/teams.csv")
    with open(f"{CLEANED_DIR}/matchups.csv", "a+") as f:
        writer = csv.writer(f, delimiter=',', quotechar="|", quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["home_team_idx", "away_team_idx", "home_score", "away_score", "year"])
        for file in os.listdir(DATA_DIR):
            full_path = f"{os.getcwd()}/{DATA_DIR}/{file}"
            df = pd.read_csv(full_path)
            row_count = df.count()[0,0]
            for idx in range(row_count):
                home_team = df[idx, 1]

                for match in list(df[idx])[2:]:
                    score = match[0]
                    home_score = None
                    away_score = None
                    away_team = match.name
                    home_team_idx = teams.filter(pd.col('team_name') == home_team)["team_id"][0]
                    abbrv_list = list(teams.filter(pd.col('abbrv') == away_team.strip())["team_id"])
                    away_team_idx = abbrv_list[0]
                    if score:
                        s = "".join(score.split())
                        sp = s.split("–")
                        try:
                            home_score = sp[0]
                            away_score = sp[1]
                        except:
                            pass

                    print(f"{home_team} {home_score} : {away_score} {away_team}: {score}, file: {file}")
                    year = file.split('.')[0]
                    if '_' in year:
                        year = year.split('_')[0]

                    writer.writerow([home_team_idx, away_team_idx, home_score, away_score, year])


def create_teams_csv():
    teams = get_teams()
    teams_dict = {}

    for i, team in enumerate(teams):
        if team in ["AZ", "VV Alkmaar"]:
            teams_dict[team] = teams_dict["Alkmaar"]
            continue

        if team in ["FC Zwolle"]:
            teams_dict[team] = teams_dict["PEC Zwolle"]
            continue

        if team in ["SC Telstar VVNH"]:
            teams_dict[team] = teams_dict["Telstar"]
            continue

        if team in ["sc Heerenveen"]:
            teams_dict[team] = teams_dict["Heerenveen"]
            continue

        if team in ["Excelsior"]:
            teams_dict[team] = teams_dict["Excelsior/Barendrecht"]
            continue


        teams_dict[team] = i

    df = pd.DataFrame({
        "team_name": teams_dict.keys(),
        "team_id": teams_dict.values(),
    })

    df.write_csv(f"{CLEANED_DIR}/teams.csv")


def get_teams():
    teams = []
    for file in os.listdir(DATA_DIR):
        try:
            full_path = f"{os.getcwd()}/{DATA_DIR}/{file}"
            df = pd.read_csv(full_path, separator=",")
            _t = list(df["Thuis \ Uit"])
            teams = teams + _t
        except:
            print(full_path)
            continue

    teams = list(dict.fromkeys(teams))
    return teams

main()