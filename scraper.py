import pandas as pd

MAIN_URL = 'https://nl.wikipedia.org/wiki/Eredivisie'
POST_FIX = '(vrouwenvoetbal)'

DATA_DIR="data"

def main():
    urls_before, urls_after = get_ranges()

    for obj in urls_before:
        tables = pd.read_html(obj['url'])
        year = obj["year"]
        if year in ["2008", "2009", "2010"]:
            tables[6].to_csv(f'{DATA_DIR}/{year}_1.csv')
            tables[7].to_csv(f'{DATA_DIR}/{year}_2.csv')
            continue

        tables[5].to_csv(f'{DATA_DIR}/{year}_1.csv')
        tables[6].to_csv(f'{DATA_DIR}/{year}_2.csv')

    for obj in urls_after:
        tables = pd.read_html(obj['url'])
        year = obj["year"]
        if year == "2015":
            tables[6].to_csv(f'{DATA_DIR}/{year}_1.csv')
            tables[7].to_csv(f'{DATA_DIR}/{year}_2.csv')
        if year == "2016":
            tables[7].to_csv(f'{DATA_DIR}/{year}_1.csv')
            tables[8].to_csv(f'{DATA_DIR}/{year}_2.csv')
            tables[11].to_csv(f'{DATA_DIR}/{year}_3.csv')
            tables[13].to_csv(f'{DATA_DIR}/{year}_4.csv')
        if year in ["2017", "2018"]:
            tables[5].to_csv(f'{DATA_DIR}/{year}_1.csv')
            tables[7].to_csv(f'{DATA_DIR}/{year}_2.csv')
            tables[9].to_csv(f'{DATA_DIR}/{year}_3.csv')
            tables[10].to_csv(f'{DATA_DIR}/{year}_4.csv')

        if year == "2019":
            tables[5].to_csv(f'{DATA_DIR}/{year}.csv')

        if year == "2020":
            tables[5].to_csv(f'{DATA_DIR}/{year}_1.csv')
            tables[7].to_csv(f'{DATA_DIR}/{year}_2.csv')
            tables[9].to_csv(f'{DATA_DIR}/{year}_3.csv')

        if int(year) >= 2021:
            tables[5].to_csv(f'{DATA_DIR}/{year}.csv')

def get_ranges():
    urls_before = []
    urls_after = []

    for year in range(7, 12):
        obj = construct_url(year)
        urls_before.append(obj)

    for year in range(15, 23):
        obj = construct_url(year)
        urls_after.append(obj)

    return (urls_before, urls_after)


def construct_url(year):
    next_year = year + 1
    if next_year < 10:
        next_year = f"0{next_year}"
    if year < 10:
        year = f"0{year}"

    _y = f"20{year}"
    url = f"{MAIN_URL}_{_y}/{next_year}_{POST_FIX}"

    obj = {
        'year': _y,
        'url': url
    }

    return obj

main()